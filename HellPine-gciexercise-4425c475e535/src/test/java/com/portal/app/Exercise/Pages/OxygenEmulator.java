package com.portal.app.Exercise.Pages;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import com.portal.app.AbstractPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class OxygenEmulator extends AbstractPage {
    //HomePage
    @FindBy(css = "#logo")
    private WebElement SITE_LOGO;
    @FindBy(css = "#top-login-form .login-button")
    private WebElement HEADER_LOGIN_BUTTON;
    @FindBy(css = "#top-login-form .join-button")
    private WebElement HEADER_JOIN_BUTTON;
    @FindBy(css = ".carousel-menu:nth-of-type(1) li:nth-of-type(6)")
    private WebElement FOOTBALL_BUTTON;
    @FindBy(css = ".btn-primary")
    private WebElement LOGIN_BUTTON;
    @FindBy(css = ".header-user-menu:nth-of-type(1) li:nth-of-type(3) a svg")
    private WebElement BETSLIP_LOGIN_BUTTON;
    @FindBy(css = ".page-wrap-inner:nth-of-type(1) .odds-btn-wrapper:nth-of-type(1) button:nth-of-type(1)")
    private WebElement MATCHES_TAB;
    @FindBy(css = ".cookie-top:nth-of-type(1) .btn-style1:nth-of-type(1)")
    private WebElement TUTORIAL_BUTTON;



    private Logger log = LogManager.getLogger(OxygenEmulator.class.getName());

    private final String logedInCookieName = "pas[galabingo][real][isOnline]";

    WebDriverWait wait = new WebDriverWait(driver,30);

    public OxygenEmulator() {
        PageFactory.initElements(driver, this);
    }

    public void openPage() {
        super.goTo("https://bet.coral.co.uk/#/");
    }


    public void footballNavigate(){
        log.info("Navigating to Football Page");
        FOOTBALL_BUTTON.click();
        ATUReports.add("FOOTBALL PAGE LOADED", LogAs.PASSED,null);
    }

    public void loginClick(){
        log.info("Tap on Login button");
        LOGIN_BUTTON.click();
        ATUReports.add("LOGIN FORM LOADED", LogAs.PASSED,null);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public Boolean isUserLogout(){
        try{
            wait.until(ExpectedConditions.visibilityOf(LOGIN_BUTTON));
            Assert.assertTrue(LOGIN_BUTTON.isDisplayed(),"User is Logged In");
            ATUReports.add("USER IS LOGGED OUT", LogAs.PASSED,null);
            return true;
        }catch (Exception ex){
            log.info(ex);
            ATUReports.add("USER IS STILL LOGGED IN", LogAs.FAILED,new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }
    }

    public void betslipClick(){
        log.info("Tapping on Betslip");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("header-user-menu:nth-of-type(1) li:nth-of-type(3) a svg")));
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].scrollIntoView()", BETSLIP_LOGIN_BUTTON);
        BETSLIP_LOGIN_BUTTON.click();
        ATUReports.add("BETSLIP OPENED", LogAs.PASSED,null);
}

    public void tutorialClick(){
        log.info("Tapping on Tutorial");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".cookie-top:nth-of-type(1) .btn-style1:nth-of-type(1)")));
        TUTORIAL_BUTTON.click();
        ATUReports.add("BETSLIP OPENED", LogAs.PASSED,null);
    }


    public void footballbetPlace(){
        log.info("Placing a bet");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".page-wrap-inner:nth-of-type(1) .odds-btn-wrapper:nth-of-type(1) button:nth-of-type(1)")));
        MATCHES_TAB.click();
        ATUReports.add("OPTION SELECTED", LogAs.PASSED,null);
    }




}
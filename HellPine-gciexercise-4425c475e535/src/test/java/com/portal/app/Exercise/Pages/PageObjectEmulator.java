package com.portal.app.Exercise.Pages;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import com.portal.app.AbstractPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PageObjectEmulator extends AbstractPage {
    //HomePage
    @FindBy(css = "#logo")
    private WebElement SITE_LOGO;
    @FindBy(css = "#top-login-form .login-button")
    private WebElement HEADER_LOGIN_BUTTON;
    @FindBy(css = "#top-login-form .join-button")
    private WebElement HEADER_JOIN_BUTTON;
    @FindBy(css = ".carousel-menu:nth-of-type(1) [href*=\"football\"]:nth-of-type(1)")
    private WebElement FOOTBALL_BUTTON;
    @FindBy(css = ".btn-primary")
    private WebElement LOGIN_BUTTON;
    @FindBy(css = ".ng-pristine [name*=\"username\"]:nth-of-type(1)")
    private WebElement USERNAME_FIELD;
    @FindBy(css = ".pass-field [name*=\"password\"]:nth-of-type(1)")
    private WebElement PASSWORD_FIELD;
    @FindBy(css = ".btn-panel [data-crlat*=\"loginButton\"]:nth-of-type(1)")
    private WebElement BETSLIP_LOGIN_BUTTON;
    @FindBy(css = ".user-login-menu li:nth-of-type(5)")
    private WebElement ACCOUNT_BALANCE_BUTTON;
    @FindBy(css = ".user-menu a:nth-of-type(13)")
    private WebElement LOGOUT_BUTTON;




    private Logger log = LogManager.getLogger(PageObjectEmulator.class.getName());

    private final String logedInCookieName = "pas[galabingo][real][isOnline]";

    WebDriverWait wait = new WebDriverWait(driver,30);

    public PageObjectEmulator() {
        PageFactory.initElements(driver, this);
    }

    public void openPage() {
        super.goTo("https://bet.coral.co.uk/#/");
    }


    public void footballNavigate(){
        log.info("Navigating to Football Page");
        FOOTBALL_BUTTON.click();
        ATUReports.add("FOOTBALL PAGE LOADED", LogAs.PASSED,null);
    }

    public void loginClick(){
        log.info("Tap on Login button");
        LOGIN_BUTTON.click();
        ATUReports.add("LOGIN FORM LOADED", LogAs.PASSED,null);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void userLogin(){
        log.info("Navigating to Football Page");
        USERNAME_FIELD.sendKeys("uatbettingauto1");
        PASSWORD_FIELD.sendKeys("example");
        BETSLIP_LOGIN_BUTTON.click();
        ATUReports.add("FOOTBALL PAGE LOADED", LogAs.PASSED,null);
    }

    public Boolean isUserLogin(){
        try{
            wait.until(ExpectedConditions.visibilityOf(ACCOUNT_BALANCE_BUTTON));
            Assert.assertTrue(ACCOUNT_BALANCE_BUTTON.isDisplayed(),"User is Logged In");
            ATUReports.add("USER IS LOGGED IN", LogAs.PASSED,null);
            return true;
        }catch (Exception ex){
            log.info(ex);
            ATUReports.add("USER IS NOT LOGGED IN", LogAs.FAILED,new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }
    }


    public void userLogout(){
        log.info("Logging Out from Account");
        ACCOUNT_BALANCE_BUTTON.click();
        LOGOUT_BUTTON.click();
        ATUReports.add("USER IS LOGGED OUT", LogAs.PASSED,null);

}

    public Boolean isUserLogout(){
        try{
            wait.until(ExpectedConditions.visibilityOf(LOGIN_BUTTON));
            Assert.assertTrue(LOGIN_BUTTON.isDisplayed(),"User is Logged In");
            ATUReports.add("USER IS LOGGED OUT", LogAs.PASSED,null);
            return true;
        }catch (Exception ex){
            log.info(ex);
            ATUReports.add("USER IS STILL LOGGED IN", LogAs.FAILED,new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }
    }
}
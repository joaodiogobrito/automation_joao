package com.portal.app.Exercise.Tests;

import com.portal.app.AbstractTest;
import com.portal.app.Exercise.Pages.PageObjectEmulator;
import com.portal.app.RetryAnalyzerImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import spring.User;

public class TestCase3 extends AbstractTest {
    private Logger log = LogManager.getLogger(TestCase3.class.getName());

    private final String LOGOUT_POPUP_TITLE_FIRST = "Logout";
    private final String LOGOUT_POPUP_MESSAGE_FIRST = "Are you sure you want to logout?";
    private final String LOGOUT_POPUP_TITLE_SECOND = "Signed Out";
    private final String LOGOUT_POPUP_MESSAGE_SECOND = "You have been logged out. Please log in again.";

    @Autowired
    @Qualifier("testBean")
    private User user;

    @org.testng.annotations.Test(retryAnalyzer=RetryAnalyzerImpl.class)
    public void loginLogout() {

        PageObjectEmulator po = new PageObjectEmulator();
        po.openPage();
        po.loginClick();
        po.userLogin();
        po.isUserLogin();
        po.userLogout();

    }

}

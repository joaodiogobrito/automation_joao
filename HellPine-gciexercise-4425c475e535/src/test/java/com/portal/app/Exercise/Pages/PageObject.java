package com.portal.app.Exercise.Pages;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import com.portal.app.AbstractPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PageObject extends AbstractPage {
    //HomePage
    @FindBy(css = "#logo")
    private WebElement SITE_LOGO;
    @FindBy(css = "#top-login-form .login-button")
    private WebElement HEADER_LOGIN_BUTTON;
    @FindBy(css = "#top-login-form .join-button")
    private WebElement HEADER_JOIN_BUTTON;
    @FindBy(css = ".quick-links-menu:nth-of-type(1) li:nth-of-type(2) a")
    private WebElement FOOTBALL_BUTTON;
    @FindBy(css = ".btn-cookie)")
    private WebElement COOKIE_BUTTON;
    @FindBy(css = ".second-tabs-content:nth-of-type(2) div:nth-of-type(5) span:nth-of-type(1)")
    private WebElement BET_BUTTON;





    private Logger log = LogManager.getLogger(com.portal.app.Exercise.Pages.PageObject.class.getName());

    private final String logedInCookieName = "pas[galabingo][real][isOnline]";

    WebDriverWait wait = new WebDriverWait(driver,30);

    public PageObject() {
        PageFactory.initElements(driver, this);
    }

    public void openPage() {
        super.goTo("http://www.coral.co.uk");
    }

    public Boolean isCoral(){
        log.info("Checking if Login and Join Buttons are present");
        try{
            wait.until(ExpectedConditions.visibilityOf(HEADER_LOGIN_BUTTON));
            Assert.assertTrue(HEADER_JOIN_BUTTON.isDisplayed(),"Login Button is there but Join Button not");
            ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE PRESENT", LogAs.PASSED,null);
            return true;
        }catch (Exception ex){
            log.info(ex);
            ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE NOT PRESENT", LogAs.FAILED,new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }
    }

    public void splashClose(){
        log.info("Closing Splash Page");
        SITE_LOGO.click();
        ATUReports.add("SPLASH PAGE LOGO CLICKED", LogAs.PASSED,null);
    }

    public void footballNavigate(){
        log.info("Navigating to Football Page");
        FOOTBALL_BUTTON.click();
        ATUReports.add("FOOTBALL PAGE LOADED", LogAs.PASSED,null);
    }

    public void cookieClear(){
        log.info("Clearing Cookie");
        COOKIE_BUTTON.click();
        ATUReports.add("COOKIE ACCEPTED", LogAs.PASSED,null);
    }


    public void betPlace(){
        log.info("Placing a bet");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".second-tabs-content:nth-of-type(1) div:nth-of-type(5) span:nth-of-type(1)")));
        BET_BUTTON.click();
        ATUReports.add("OPTION SELECTED", LogAs.PASSED,null);
    }



}
# README #

Theoric Responses:
1. Have one correct account credentials, verify all the locators, verify each test step (e.g. login, verify if user is logged in, logout, verify if user is logged out),
contemplate other possible issues such as cookies, tutorial page, ticked/not-ticked boxes such as "remember me" and have a report with the results.
2.  The main difference between  them is that Selenium is mainly used for testing browser based application on computers while appium is used for devices/native applications.
3. Implicit wait is a method used to have a driver wait while a process is being loaded/rendered, it can be relative due to its dependency on the process load time,
Explicit wait is a method used to make the driver wait for one determined time, expressed inside the method, and it takes always that absolute time.
4. Page Object are a framework that creates an archicteture where is possible to with less changes create the automated tests. Each page is represented by a java class that 
has all the required locators and methods needed to create the driver steps.
5. Jenkins is a software where is possible to create jobs related to different testing builds. It has also the option to connect to  other important softwares such as Test Rail and Jira, among others. 
6. "Version Control System" are repositories where is possible to commit builds. These are very useful because they can be accessed anywhere, anytime, among other functionalities.
The ones that I know are Github and Bitbucket. 
7. Acceptance is when a result is according to defined criteria. In automation perspective it compares actual with expected result. After executing the test a risk impact 
analysis is made and if it satisfies the criteria than it is accepted.
8. Smoke test is the first test made as soon as the release deployment is finished and verifies critical areas of the system at test. Sanity test is performed to verify new 
functionalities and is made to stable build . Regression test is the test made to all the areas of the system that were not changed.
9. The scope of automation is any possible task that can be done without human intervention. Scope of automation can be affected by priorities/ risk management. 
10. GET and POST are both http methods, the diffenrences between them have to do with the type of information dealt, cache differences, encoding type, restrictions to data
length and type. GET would be used to any not sensitive information while POST deals with more sensitive information.